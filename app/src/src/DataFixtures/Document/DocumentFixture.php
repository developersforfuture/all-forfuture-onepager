<?php

namespace App\DataFixtures\Document;

use App\DataFixtures\ORM\AppFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use RuntimeException;
use Sulu\Bundle\DocumentManagerBundle\DataFixtures\DocumentFixtureInterface;
use League\Csv\Reader;
use Sulu\Bundle\MediaBundle\Entity\Media;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;
use Sulu\Bundle\PageBundle\Document\PageDocument;
use Sulu\Component\Content\Document\RedirectType;
use Sulu\Component\Content\Document\WorkflowStage;
use Sulu\Component\DocumentManager\DocumentManager;
use Sulu\Component\DocumentManager\Exception\DocumentManagerException;
use Sulu\Component\DocumentManager\Exception\MetadataNotFoundException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sulu\Component\PHPCR\PathCleanup;

class DocumentFixture implements DocumentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function getOrder(): int
    {
        return 10;
    }

    private function getEntityManager(): EntityManagerInterface
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->entityManager;
    }


    private function getMediaId(string $name): int
    {
        try {
            $id = $this->getEntityManager()->createQueryBuilder()
                ->from(Media::class, 'media')
                ->select('media.id')
                ->innerJoin('media.files', 'file')
                ->innerJoin('file.fileVersions', 'fileVersion')
                ->where('fileVersion.name = :name')
                ->setMaxResults(1)
                ->setParameter('name', $name)
                ->getQuery()->getSingleScalarResult();

            return (int) $id;
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException(sprintf('Too many images with the name "%s" found.', $name), 0, $e);
        }
    }

        /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @throws DocumentManagerException
     */
    private function loadHomepage(DocumentManager $documentManager): void
    {
        /** @var HomeDocument $homeDocument */
        $homeDocument = $documentManager->find('/cmf/forfuture/contents', AppFixtures::LOCALE_EN);

        $reader = Reader::createFromPath(
            __DIR__.'/../../Resources/data/organisations.csv'
        );
        $reader->setHeaderOffset(0);
        $reader->setEnclosure('"');
        $reader->setDelimiter(',');
        /** @var \Iterator $results */
        $results = $reader->getRecords(['title', 'link', 'logo', 'mail', 'description', 'twitter', 'a', 'b']);
        foreach ($results as $row) {
            if (!empty($row['logo'])) {
                $row['type'] = 'logo';
                $pathInfo = pathinfo($row['logo']);
                $logoFileName = $pathInfo['basename'];
                try {
                    $row['logo'] = [$this->getMediaId($logoFileName)];
                } catch (\Exception $e) {
                    var_dump('No image persisted for: '.$row['logo']);
                }
            }

            $logos[] = $row;
        }

        $homeDocument->getStructure()->bind(
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => $homeDocument->getTitle(),
                'structureType' => 'homepage',
                'parent_path' => '/cmf/forfuture/contents',
                'intro' => 'Engagiere dich in einer der zahlreichen for-Future-Gruppen! Hier findest
                    du eine Übersicht der Erwachseneninitiativen, die sich hinter die
                    Fridays for Future Bewegung stellen und diese unterstützen. Werde aktiv
                    und setze dich für einen echten Klimaschutz ein!',
                'url' => '/',
                'seo' => [
                    'title' => '',
                    'description' => '',
                    'description' => '',
                ],
                'logos' => $logos,
            ]
        );

        $documentManager->persist($homeDocument, AppFixtures::LOCALE_DE);
    }

    private function loadPages(DocumentManager $documentManager): array
    {
        $pages =  [
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Impressum',
                'structureType' => 'default',
                'url' => '/impressum',
                'parent_path' => '/cmf/forfuture/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'AlleFürsKlima - Impressum',
                    'description' => 'Die Pflichtseite.',
                ],
                'article' => '
                    <h4>Impressum</h4>
                    <div></div>
                    <div><span style="color: #070707;">Angaben gemäß § 5 TMG</span></div>
                    <div><span style="color: #070707;"> </span></div>
                    <div><span style="color: #070707;">Together for Future e.V. </span></div>
                    <div><span style="color: #070707;">Gaudystraße 14 </span></div>
                    <div><span style="color: #070707;">10437 Berlin</span></div>
                    <div><span style="color: #070707;"> </span></div>
                    <div><span style="color: #070707;">Vertreten durch: </span></div>
                    <div><span style="color: #070707;">Herr Torben Greve (erster Vorstand), Frau Anna Schwanhäußer (stellvertretender Vorstand)</span></div>
                    <div><span style="color: #070707;"><b> </b></span></div>
                    <div><span style="color: #070707;"><b>Kontakt</b></span></div>
                    <div><span style="color: #070707;">E-Mail: kontakt@togetherforfuture.net</span></div>
                    <div><span style="color: #070707;">Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.</span>
                    </div>
                    <div><span style="color: #070707;"> </span></div>
                    <div><span style="color: #070707;"><b>Haftung für Inhalte</b></span></div>
                    <div><span style="color: #070707;">Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</span>
                    </div>
                    <div><span style="color: #070707;">Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</span>
                    </div>
                    <div><span style="color: #070707;"><b> </b></span></div>
                    <div><span style="color: #070707;"><b>Haftung für Links</b></span></div>
                    <div><span style="color: #070707;">Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</span>
                    </div>
                    <div><span style="color: #070707;">Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer </span>Rechtsverletzung
                        nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
                    </div>
                    <div><b> </b></div>
                    <div><span style="color: #070707;"><b>Urheberrecht</b></span></div>
                    <div><span style="color: #070707;">Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</span>
                    </div>
                    <div><span style="color: #070707;">Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</span>
                '
            ],
            [
                'locale' => AppFixtures::LOCALE_DE,
                'title' => 'Datenschutz',
                'structureType' => 'default',
                'url' => '/datenschutz',
                'parent_path' => '/cmf/forfuture/contents',
                'navigationContexts' =>  ['footer'],
                'seo' => [
                    'title' => 'AlleFürsKlima Datenschutz',
                    'description' => 'Alles rund um den Datenschutz',
                ],
                'article' => '
                    <h4>Datenshutz</h4>
                    <div id="1270035459" class="u_1270035459 dmNewParagraph" data-dmtmpl="true" data-uialign="center">
                    <div></div>
                    <div><span style="color: #383838;">Allgemeine Hinweise</span></div>
                    <div><span style="color: #383838;">Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit Ihren personenbezogenen Daten passiert, wenn Sie unsere Website besuchen. Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden können. Ausführliche Informationen zum Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten Datenschutzerklärung.</span></div>
                    <div><span style="color: #383838;">Datenerfassung auf unserer Website</span></div>
                    <div><span style="color: #383838;">Wer ist verantwortlich für die Datenerfassung auf dieser Website?</span></div>
                    <div><span style="color: #383838;">Die Datenverarbeitung auf dieser Website erfolgt durch den Websitebetreiber. Dessen Kontaktdaten können Sie dem Impressum dieser Website entnehmen.</span></div>
                    <div><span style="color: #383838;">Wie erfassen wir Ihre Daten?</span></div>
                    <div><span style="color: #383838;">Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese mitteilen. Hierbei kann es sich z. B. um Daten handeln, die Sie in ein Kontaktformular eingeben.</span></div>
                    <div><span style="color: #383838;">Andere Daten werden automatisch beim Besuch der Website durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten (z. B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie unsere Website betreten.</span></div>
                    <div><span style="color: #383838;">Wofür nutzen wir Ihre Daten?</span></div>
                    <div><span style="color: #383838;">Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Website zu gewährleisten. Andere Daten können zur Analyse Ihres Nutzerverhaltens verwendet werden.</span></div>
                    <div><span style="color: #383838;">Welche Rechte haben Sie bezüglich Ihrer Daten?</span></div>
                    <div><span style="color: #383838;">Sie haben jederzeit das Recht unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Sie haben außerdem ein Recht, die Berichtigung, Sperrung oder Löschung dieser Daten zu verlangen. Hierzu sowie zu weiteren Fragen zum Thema Datenschutz können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden. Des Weiteren steht Ihnen ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu.</span></div>
                    <div><span style="color: #383838;">Außerdem haben Sie das Recht, unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Details hierzu entnehmen Sie der Datenschutzerklärung unter „Recht auf Einschränkung der Verarbeitung“.</span></div>
                    <div><span style="color: #383838;">Analyse-Tools und Tools von Drittanbietern</span></div>
                    <div><span style="color: #383838;">Beim Besuch unserer Website kann Ihr Surf-Verhalten statistisch ausgewertet werden. Das geschieht vor allem mit Cookies und mit sogenannten Analyseprogrammen. Die Analyse Ihres Surf-Verhaltens erfolgt in der Regel anonym; das Surf-Verhalten kann nicht zu Ihnen zurückverfolgt werden.</span></div>
                    <div><span style="color: #383838;">Sie können dieser Analyse widersprechen oder sie durch die Nichtbenutzung bestimmter Tools verhindern. Detaillierte Informationen zu diesen Tools und über Ihre Widerspruchsmöglichkeiten finden Sie in der folgenden Datenschutzerklärung.</span></div>
                    <div></div>
                    <h4><span style="color: #383838;">2. Allgemeine Hinweise und Pflichtinformationen</span></h4>
                    <div><span style="color: #383838;">Datenschutz</span></div>
                    <div><span style="color: #383838;">Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.</span></div>
                    <div><span style="color: #383838;">Wenn Sie diese Website benutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie persönlich identifiziert werden können. Die vorliegende Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.</span></div>
                    <div><span style="color: #383838;">Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</span></div>
                    <div><span style="color: #383838;">Hinweis zur verantwortlichen Stelle</span></div>
                    <div><span style="color: #383838;">Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:</span></div>
                    <div><span style="color: #383838;">Together for Future e.V. </span></div>
                    <div><span style="color: #383838;">Gaudystraße 14 </span></div>
                    <div><span style="color: #383838;">10437 Berlin</span></div>
                    <div><span style="color: #383838;"> </span></div>
                    <div><span style="color: #383838;">E-Mail: kontakt@togetherforfuture.net</span></div>
                    <div><span style="color: #383838;"> </span></div>
                    <div><span style="color: #383838;">Verantwortliche Stelle ist die natürliche oder juristische Person, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z. B. Namen, E-Mail-Adressen o. Ä.) entscheidet.</span></div>
                    <div><span style="color: #383838;"> </span></div>
                    <div><span style="color: #383838;">Widerruf Ihrer Einwilligung zur Datenverarbeitung</span></div>
                    <div><span style="color: #383838;">Viele Datenverarbeitungsvorgänge sind nur mit Ihrer ausdrücklichen Einwilligung möglich. Sie können eine bereits erteilte Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.</span></div>
                    <div><span style="color: #383838;"> </span></div>
                    <div><span style="color: #383838;"><b>Widerspruchsrecht gegen die Datenerhebung in besonderen Fällen sowie gegen Direktwerbung (Art. 21 DSGVO)</b></span></div>
                    <div><span style="color: #383838;"><b> </b></span></div>
                    <div><span style="color: #383838;"><b>Wenn die Datenverarbeitung auf Grundlage von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, haben Sie jederzeit das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, gegen die Verarbeitung Ihrer personenbezogenen Daten Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Die jeweilige Rechtsgrundlage, auf denen eine Verarbeitung beruht, entnehmen Sie dieser Datenschutzerklärung. Wenn Sie Widerspruch einlegen, werden wir Ihre betroffenen personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen (Widerspruch nach Art. 21 Abs. 1 DSGVO).</b></span></div>
                    <div><span style="color: #383838;"><b>Werden Ihre personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, so haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung Sie betreffender personenbezogener Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Wenn Sie widersprechen, werden Ihre personenbezogenen Daten anschließend nicht mehr zum Zwecke der Direktwerbung verwendet (Widerspruch nach Art. 21 Abs. 2 DSGVO).</b></span></div>
                    <div><span style="color: #383838;"> </span></div>
                    <div><span style="color: #383838;">Beschwerderecht bei der zuständigen Aufsichtsbehörde</span></div>
                    <div><span style="color: #383838;">Im Falle von Verstößen gegen die DSGVO steht den Betroffenen ein Beschwerderecht bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres gewöhnlichen Aufenthalts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes zu. Das Beschwerderecht besteht unbeschadet anderweitiger verwaltungsrechtlicher oder gerichtlicher Rechtsbehelfe.</span></div>
                    <div><span style="color: #383838;">Recht auf Datenübertragbarkeit</span></div>
                    <div><span style="color: #383838;">Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an einen Dritten in einem gängigen, maschinenlesbaren Format aushändigen zu lassen. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</span></div>
                    <div><span style="color: #383838;">SSL- bzw. TLS-Verschlüsselung</span></div>
                    <div><span style="color: #383838;">Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL-, bzw. TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.</span></div>
                    <div><span style="color: #383838;">Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.</span></div>
                    <div><span style="color: #383838;">Auskunft, Sperrung, Löschung und Berichtigung</span></div>
                    <div><span style="color: #383838;">Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.</span></div>
                    <div><span style="color: #383838;">Recht auf Einschränkung der Verarbeitung</span></div>
                    <div><span style="color: #383838;">Sie haben das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Hierzu können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden. Das Recht auf Einschränkung der Verarbeitung besteht in folgenden Fällen:</span></div>
                    <div><span style="color: #383838;">Wenn Sie die Richtigkeit Ihrer bei uns gespeicherten personenbezogenen Daten bestreiten, benötigen wir in der Regel Zeit, um dies zu überprüfen. Für die Dauer der Prüfung haben Sie das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Wenn die Verarbeitung Ihrer personenbezogenen Daten unrechtmäßig geschah/geschieht, können Sie statt der Löschung die Einschränkung der Datenverarbeitung verlangen. Wenn wir Ihre personenbezogenen Daten nicht mehr benötigen, Sie sie jedoch zur Ausübung, Verteidigung oder Geltendmachung von Rechtsansprüchen benötigen, haben Sie das Recht, statt der Löschung die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Wenn Sie einen Widerspruch nach Art. 21 Abs. 1 DSGVO eingelegt haben, muss eine Abwägung zwischen Ihren und unseren Interessen vorgenommen werden. Solange noch nicht feststeht, wessen Interessen überwiegen, haben Sie das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</span></div>
                    <div><span style="color: #383838;">Wenn Sie die Verarbeitung Ihrer personenbezogenen Daten eingeschränkt haben, dürfen diese Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Europäischen Union oder eines Mitgliedstaats verarbeitet werden.</span></div>
                    <div><span style="color: #383838;">Widerspruch gegen Werbe-E-Mails</span></div>
                    <div><span style="color: #383838;">Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor. </span></div>
                    <div></div>
                    <!--
                    <h4><span style="color: #383838;">3. Datenerfassung auf unserer Website</span></h4>
                    <div><span style="color: #383838;">Kontaktformular</span></div>
                    <div><span style="color: #383838;">Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</span></div>
                    <div><span style="color: #383838;">Die Verarbeitung der in das Kontaktformular eingegebenen Daten erfolgt somit ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Sie können diese Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.</span></div>
                    <div><span style="color: #383838;">Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende gesetzliche Bestimmungen – insbesondere Aufbewahrungsfristen – bleiben unberührt.</span></div>
                    <div><span style="color: #383838;">Anfrage per E-Mail, Telefon oder Telefax</span></div>
                    <div><span style="color: #383838;">Wenn Sie uns per E-Mail, Telefon oder Telefax kontaktieren, wird Ihre Anfrage inklusive aller daraus hervorgehenden personenbezogenen Daten (Name, Anfrage) zum Zwecke der Bearbeitung Ihres Anliegens bei uns gespeichert und verarbeitet. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</span></div>
                    <div><span style="color: #383838;">Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) und/oder auf unseren berechtigten Interessen (Art. 6 Abs. 1 lit. f DSGVO), da wir ein berechtigtes Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen haben.</span></div>
                    <div><span style="color: #383838;">Die von Ihnen an uns per Kontaktanfragen übersandten Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere gesetzliche Aufbewahrungsfristen – bleiben unberührt. </span></div>
                    <div><span style="color: #383838;"> </span></div>
                    -->
                '
            ]
        ];
        foreach ($pages as $pageData) {
            $this->createPage($documentManager, $pageData);
        }

        return $pages;
    }

    private function getPathCleanup(): PathCleanup
    {
        if (null === $this->pathCleanup) {
            $this->pathCleanup = $this->container->get('sulu.content.path_cleaner');
        }

        return $this->pathCleanup;
    }


        /**
     * @param mixed[] $data
     *
     * @throws MetadataNotFoundException
     */
    private function createPage(DocumentManager $documentManager, array $data): BasePageDocument
    {
        $locale = isset($data['locale']) && $data['locale'] ? $data['locale'] : AppFixtures::LOCALE_DE;

        if (!isset($data['url'])) {
            $url = $this->getPathCleanup()->cleanup('/' . $data['title']);
            if (isset($data['parent_path'])) {
                $url = mb_substr($data['parent_path'], mb_strlen('/cmf/developersforfuture/contents')) . $url;
            }

            $data['url'] = $url;
        }

        $extensionData = [
            'seo' => $data['seo'] ?? [],
            'excerpt' => $data['excerpt'] ?? [],
        ];

        unset($data['excerpt']);
        unset($data['seo']);

        /** @var PageDocument $pageDocument */
        $pageDocument = null;

        try {
            if (!isset($data['id']) || !$data['id']) {
                throw new Exception();
            }

            $pageDocument = $documentManager->find($data['id'], $locale);
        } catch (Exception $e) {
            $pageDocument = $documentManager->create('page');
        }

        $pageDocument->setNavigationContexts($data['navigationContexts'] ?? []);
        $pageDocument->setLocale($locale);
        $pageDocument->setTitle($data['title']);
        $pageDocument->setResourceSegment($data['url']);
        $pageDocument->setStructureType($data['structureType'] ?? 'default');
        $pageDocument->setWorkflowStage(WorkflowStage::PUBLISHED);
        $pageDocument->getStructure()->bind($data);
        $pageDocument->setAuthor(1);
        $pageDocument->setExtensionsData($extensionData);

        if (isset($data['redirect'])) {
            $pageDocument->setRedirectType(RedirectType::EXTERNAL);
            $pageDocument->setRedirectExternal($data['redirect']);
        }

        $documentManager->persist(
            $pageDocument,
            $locale,
            ['parent_path' => $data['parent_path'] ?? '/cmf/forfuture/contents']
        );

        // Set dataSource to current page after persist as uuid is before not available
        if (isset($data['pages']['dataSource']) && '__CURRENT__' === $data['pages']['dataSource']) {
            $pageDocument->getStructure()->bind(
                [
                    'pages' => array_merge(
                        $data['pages'],
                        [
                            'dataSource' => $pageDocument->getUuid(),
                        ]
                    ),
                ]
            );

            $documentManager->persist(
                $pageDocument,
                $locale,
                ['parent_path' => $data['parent_path'] ?? '/cmf/forfuture/contents']
            );
        }

        $documentManager->publish($pageDocument, $locale);

        return $pageDocument;
    }

      /**
     * @throws DocumentManagerException
     * @throws MetadataNotFoundException
     * @throws Exception
     */
    public function load(DocumentManager $documentManager): void
    {
        $this->loadHomepage($documentManager);
        $this->loadPages($documentManager);
        #$this->loadContactSnippet($documentManager);
        // Needed, so that a Document use by loadHomepageGerman is managed.
        $documentManager->flush();
        #$this->updatePages($documentManager, AppFixtures::LOCALE_EN);

        $documentManager->flush();
    }
}
