#!/usr/bin/env sh

set -ex

GIT_TAG='4ca828a953a18b7985c6c1c6b850740a17dd73d1'
TAG=${1-'4ca828a953a18b7985c6c1c6b850740a17dd73d1'}
DOCKER_IMAGE=${DOCKER_IMAGE-'local'}
if [ "${DOCKER_IMAGE}" != 'local' ]
then
    # try to pull previously built images to utilize docker cache during new build
    docker pull $DOCKER_IMAGE/composer:latest || true
    docker pull $DOCKER_IMAGE/node-admin:latest || true
    docker pull $DOCKER_IMAGE/node-website:latest || true
    docker pull $DOCKER_IMAGE/server:latest || true
fi;


# build and tag intermediate images and application image
docker build . --target composer -t $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/composer:latest
docker build . --target node-admin -t $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest
docker build . --target node-website -t $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest
docker build . --target server -t $DOCKER_IMAGE/server:latest \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/server:latest 
docker build . --target production -t $DOCKER_IMAGE/production:$VERSION_TAG \
    --cache-from $DOCKER_IMAGE/composer:latest \
    --cache-from $DOCKER_IMAGE/node-admin:latest \
    --cache-from $DOCKER_IMAGE/node-website:latest \
    --cache-from $DOCKER_IMAGE/server:latest

    docker build . --target production -t local/production:sulu_start \
    --cache-from local/composer:latest \
    --cache-from local/node-admin:latest \
    --cache-from local/node-website:latest \
    --cache-from local/server:latest
if [ ! -z "${DOCKER_IMAGE}" ]
then
# PUSH INTO REGISTRY
    docker push $DOCKER_IMAGE/composer:latest
    docker push $DOCKER_IMAGE/node-admin:latest
    docker push $DOCKER_IMAGE/node-website:latest
    docker push $DOCKER_IMAGE/server:latest
    docker push $DOCKER_IMAGE/production:$VERSION_TAG
fi;