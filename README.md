# Developers For Future - All ForFuture

Currently it is a Symfony based application called [Sulu.io](https://sulu.io) with some help of Symfony CMF. To run the app locally run a:

```
git clone git@github.com:forfuture/website.git
cd website/app/src
composer install
bin/console server:run
```

## Build and Deployment

At the end we do manually build a application wide image by running:

The build is done by a CI pipeline, so simply push your stuff on your branch, build a Merge Request (MR). A merge on master will enable train of jobs to build the app. The deployment is currently done manually by @ElectriMaxxx

### Using Docker-Compose

```bash
cp .env.dist .env
# edit it for your local usage
export $(egrep -v '^#' .env | xargs)
docker-compose -f docker-compose.development.yaml up -d
```

### Assets by Webpack

As the assets are handled by webpack, you should run its whatcher when working with Sass or Typescript files. So:

```bash
cd app/src
npm run watch
```

#### Admin UI

The code running the admin got a webpack also:

```bash
cd app/src/assets/admin/
npm install
npm run build
```

Doing so, you will always get the changes.
